# Macro Calculator

**Description:** This macro calculator allows for you to find the optimal macronutrient goals for you to intake based upon your BMR (Basal Metabolic Rate), goal, and activity level.  
