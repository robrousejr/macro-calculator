// On page load
document.onload = function(){

    // Global Output elements
    var proteinOutput = document.getElementById('proteinOutput');
    var carbsOutput = document.getElementById('carbsOutput');
    var fatOutput = document.getElementById('fatOutput');
    var caloriesOutput = document.getElementById('caloriesOutput');
}


// onSubmit from macro form
function macroForm(){
    // Dropdown list elements
    var inputSelectList = document.getElementById("inputActivityLevel");
    var goalSelectList = document.getElementById("inputGoal");
    var genderSelectList = document.getElementById("inputGender");

    // User inputs
    var height = document.getElementById('inputHeight').value;
    var weight = document.getElementById('inputWeight').value;
    var activityLevel = inputSelectList.options[inputSelectList.selectedIndex].text;
    var goal = goalSelectList.options[goalSelectList.selectedIndex].text;
    var gender = genderSelectList.options[genderSelectList.selectedIndex].text;
    var age = document.getElementById('inputAge').value;

    // Validate form input
    if(height == "" || weight == "" || age == ""){
        alert("Fill in every input value");
        return;
    }

    if(height < 48 || height > 90)
    {
        alert("Enter a proper height value in between 48 and 90 inches");
        return;
    }

    if(weight < 50 || weight > 400)
    {
        alert("Enter a proper weight in between 50 and 400 pounds");
        return;
    }

    if(age < 15 || age > 80)
    {
        alert("Enter a proper age in between 15 and 80 years old");
        return;
    }


    // BMR calculation 
    if(gender == "male")
    {
        var BMR = 66 + (6.23 * weight) + (12.7 * height) - (6.8 * age);
    }
    else
    {
        var BMR = 655 + (4.35 * weight) + (4.7 * height) - (4.7 * age);
    }

    console.log("New BMR is " + BMR);

    var newRate;
    // New rate calculation 
    switch(activityLevel) {
        case "Sedentary":
            newRate = BMR * 1.2;
            break;
        case "Lightly Active":
            newRate = BMR * 1.375;
            break;
        case "Moderate":
            newRate = BMR * 1.55;
            break;
        case "Active":
            newRate = BMR * 1.725;
            break;
        case "Very Active":
            newRate = BMR * 1.9;
            break;
    }

    // Factor in goal
    switch(goal) {
        case "Lose Weight":
            newRate -= 500; 
            break;
        case "Maintain":
            break;
        case "Gain Weight":
            newRate += 300;
            break;
    }

    // Calculate macros
    var protein = Math.round((weight * 0.7)); // total protein allocated
    var fat = Math.round((weight * 0.4)); // total fat allocated 
    var caloriesLeft = newRate - ((fat * 9) + (protein * 4)); 
    var carbs = Math.round((caloriesLeft / 4));

    // Display output 
    proteinOutput.innerHTML = protein + "g";
    carbsOutput.innerHTML = carbs + "g";
    fatOutput.innerHTML = fat + "g";

    // Display total calories
    var totalCalories = ((protein * 4) + (carbs * 4) + (fat * 9));
    caloriesOutput.innerHTML += totalCalories;
}

// Clears form output back to 0
function clearForm() {

    // Reset output 
    proteinOutput.innerHTML = "0g";
    carbsOutput.innerHTML = "0g";
    fatOutput.innerHTML = "0g";
    caloriesOutput.innerHTML = "Total Calories: ";
}